<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 30-Aug-20
 * Time: 23:11
 */

namespace App\Stats;


class Health extends BaseStats
{
    public function calculateDamage($damage)
    {
        $this->value -= $damage;

        return $this;
    }
}