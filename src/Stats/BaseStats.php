<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 30-Aug-20
 * Time: 23:06
 */

namespace App\Stats;


class BaseStats
{
    protected $min = null;
    protected $max = null;
    protected $value = null;

    public function __construct($min, $max)
    {
        $this->min = $min;
        $this->max = $max;

        if($this->value === null) {
            $this->value = rand($this->min, $this->max);
        }
    }

    public function getValue()
    {
        return $this->value;
    }
}