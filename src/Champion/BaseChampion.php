<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 30-Aug-20
 * Time: 21:04
 */

namespace App\Champion;
use App\Stats\Health;
use App\Stats\Strength;
use App\Stats\Defense;
use App\Stats\Speed;
use App\Stats\Luck;

abstract class BaseChampion
{
    protected $health;
    protected $strength;
    protected $defense;
    protected $speed;
    protected $luck;
    protected $skills = [];

    abstract public static function getInstance();

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth(Health $health)
    {
        $this->health = $health;

        return $this;
    }

    public function getStrength()
    {
        return $this->strength;
    }

    public function setStrength(Strength $strength)
    {
        $this->strength = $strength;

        return $this;
    }

    public function getDefense()
    {
        return $this->defense;
    }

    public function setDefense(Defense $defense)
    {
        $this->defense = $defense;

        return $this;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function setSpeed(Speed $speed)
    {
        $this->speed = $speed;

        return $this;
    }

    public function getLuck()
    {
        return $this->luck;
    }

    public function setLuck(Luck $luck)
    {
        $this->luck = $luck;

        return $this;
    }

    public function addSkill($skill)
    {
        array_push($this->skills, $skill);
    }

    public function getSkills()
    {
        return $this->skills;
    }

    public function damage($damage)
    {
        $this->health = $this->health->calculateDamage($damage);

        return $this;
    }

    public function getNameAndHP()
    {
        return $this->getChampionName() . ' (Health:' . $this->getHealth()->getValue() . ')';
    }

    public function getChampionName() {
        return substr(strrchr(get_class($this), "\\"), 1);
    }

    public function activateSkills($role)
    {
        echo nl2br($this->getChampionName() .  ' ' . $role . " - ");
        $this->attemptSkillsActivationByRole($role);
    }

    public function attemptSkillsActivationByRole($role)
    {
        foreach ($this->skills as &$skill) {
            if ($skill->getRole() == $role) {
                $skill->activate();
            }
        }
    }

    public function useActiveSkills($callback)
    {
        $noSkillsAvailable = true;
        foreach ($this->skills as &$skill) {
            if ($skill->getSkillActive()) {
                $noSkillsAvailable = false;
                $skill->useSkill(function () use ($callback) {
                    return $callback();
                });
            }
        }

        if ($noSkillsAvailable) $callback();

        return $this;
    }
}