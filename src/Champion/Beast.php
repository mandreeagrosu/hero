<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 30-Aug-20
 * Time: 21:12
 */

namespace App\Champion;


class Beast extends BaseChampion
{
    private static $inst = null;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (self::$inst === null)
        {
            self::$inst = new Beast();
        }

        return self::$inst;
    }
}