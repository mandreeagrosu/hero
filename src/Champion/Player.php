<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 30-Aug-20
 * Time: 21:22
 */

namespace App\Champion;


class Player extends BaseChampion
{
    private static $inst = null;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (self::$inst === null)
        {
            self::$inst = new Player();
        }

        return self::$inst;
    }

    public function getChampionName() {
        return 'Orderus';
    }
}