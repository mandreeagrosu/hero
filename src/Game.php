<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 31-Aug-20
 * Time: 13:56
 */

namespace App;


class Game
{
    private static $inst = null;
    private $allowedRounds = 20;
    private $attacker = null;
    private $defender = null;
    private $damage = null;
    private $winner = null;

    public function __construct($attacker, $defender)
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
    }

    public static function getInstance($attacker = null, $defender = null)
    {
        if (self::$inst === null) {
            self::$inst = new Game($attacker, $defender);
        }

        return self::$inst;
    }

    // damage calculation rule
    public function setDamage($damage = null)
    {
        if ($damage === null) {
            $newDamage = $this->attacker->getStrength()->getValue() - $this->attacker->getDefense()->getValue();
            $this->damage = ($newDamage < 0 ? 0 : $newDamage);
        } else {
            $this->damage = $damage;
        }

        return $this;
    }

    public function getDamage()
    {
        return $this->damage;
    }

    public function battle()
    {
        $this->setFightInitiator();

        for($round = 1; $round <= $this->allowedRounds; $round++)
        {
            $this->displayMessage("ROUND " . $round);
            $this->attacker->activateSkills('attacking');
            $this->defender->activateSkills('defending');

            $this->attacker->useActiveSkills(function () {
                return $this->initiateAttack();
            });

            if($this->winner === null) {
                if($round === $this->allowedRounds) {
                    $this->displayMessage("The battle ended with a DRAW");
                } else {
                    $this->switchRoles();
                }
            } else {
                $this->displayMessage('');
                $this->displayMessage("The winner is " . $this->attacker->getNameAndHP());
                exit;
            }
        }
    }

    private function initiateAttack()
    {
        // reduces the damage if skill is active
        $this->defender->useActiveSkills(function () {
            return $this->setDamage();
        });

        $attackChance = rand(0, 100);
        $this->displayMessage($this->attacker->getNameAndHP() . " tries to attack " . $this->defender->getNameAndHP() . " (" . $this->defender->getLuck()->getValue() .  "% chance to defend)");

        // test defenders luck
        if ($this->defender->getLuck()->getValue() <= $attackChance) {
            $this->defender->damage($this->getDamage());
            $this->displayMessage( $this->attacker->getNameAndHP() . " causes " . $this->getDamage() . " damage to " . $this->defender->getNameAndHP());
        } else {
            $this->displayMessage($this->defender->getNameAndHP() . " gets lucky! " . $this->attacker->getNameAndHP() . " misses");
        }

        if ($this->defender->getHealth()->getValue() <= 0) {
            $this->winner = $this->attacker;
        }
    }

    private function setFightInitiator() {
        $this->displayMessage("Checking speed and luck of both players.");
        $attackerSpeed = $this->attacker->getSpeed()->getValue();
        $defenderSpeed = $this->defender->getSpeed()->getValue();
        $attackerLuck = $this->attacker->getLuck()->getValue();
        $defenderLuck = $this->defender->getLuck()->getValue();

        if ($attackerSpeed === $defenderSpeed) {
            if ($attackerLuck === $defenderLuck) {
                $this->displayMessage("ERROR: Both players have similar speed and luck");
            } else {
                if ($attackerLuck > $defenderLuck) {
                    $this->displayMessage($this->attacker->getNameAndHP(). " goes first");
                    return true;
                } else {
                    $this->switchRoles();
                    $this->displayMessage( $this->defender->getNameAndHP(). " goes first");
                }
            }
        } else {
            if ($attackerSpeed > $defenderSpeed) {
                $this->displayMessage($this->attacker->getNameAndHP(). " goes first");
                return true;
            } else {
                $this->switchRoles();
                $this->displayMessage($this->defender->getNameAndHP(). " goes first");
            }
        }
    }

    private function switchRoles()
    {
        $oldAttacker = $this->attacker;
        $this->attacker = $this->defender;
        $this->defender = $oldAttacker;
        $this->displayMessage("Players have switched roles \n\r");
    }

    private function displayMessage($message){
        echo nl2br($message . "\n\r");
    }
}