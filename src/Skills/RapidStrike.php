<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 30-Aug-20
 * Time: 22:40
 */

namespace App\Skills;


class RapidStrike extends BaseSkill
{
    protected $probability = 10;
    protected $role = 'attacking';

    public function useSkill($attack)
    {
        echo nl2br('RapidStrike: Strike twice \n\r');
        $this->skill_active = false;
        $attack();
        $attack();
        return $this;
    }

}