<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 30-Aug-20
 * Time: 22:35
 */

namespace App\Skills;


class MagicShield extends BaseSkill
{
    protected $probability = 20;
    protected $role = 'defending';

    public function useSkill($damage)
    {
        $battle = $damage();
        if ($battle != null) {
            $battle->setDamage($battle->getDamage() / 2);
            echo nl2br("Magic shield 50% damage \n\r");
        }

        return $battle;
    }
}