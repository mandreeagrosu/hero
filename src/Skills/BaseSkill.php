<?php
/**
 * Created by PhpStorm.
 * User: Andreea Grosu
 * Date: 30-Aug-20
 * Time: 22:13
 */

namespace App\Skills;


class BaseSkill
{
    protected $probability = 0;
    protected $skill_active = false;
    protected $role;

    public function getProbability()
    {
        return $this->probability;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getSkillActive()
    {
        return $this->skill_active;
    }

    public function activate()
    {
        $success_value = rand(0,100);

        if($this->probability >= $success_value) {
            echo nl2br( $this->getSkillName() . " activated! \n\r");
            $this->skill_active = true;
        } else {
            echo nl2br( $this->getSkillName() . " FAILED! \n\r");
            $this->skill_active = false;
        }

        return $this;
    }

    public function getSkillName() {
        return substr(strrchr(get_class($this), "\\"), 1);
    }
}