<?php
require_once 'vendor/autoload.php';

use App\Champion\Player;
use App\Champion\Beast;
use App\Stats\Health;
use App\Stats\Strength;
use App\Stats\Defense;
use App\Stats\Speed;
use App\Stats\Luck;
use App\Skills\RapidStrike;
use App\Skills\MagicShield;
use App\Game;

// set player stats and skills
$player = Player::getInstance();
$player->setHealth(new Health(70, 100));
$player->setStrength(new Strength(70, 80));
$player->setDefense(new Defense(45, 55));
$player->setSpeed(new Speed(40, 50));
$player->setLuck(new Luck(10, 30));
$player->addSkill(new RapidStrike);
$player->addSkill(new MagicShield);

// set beasts stats
$beast = Beast::getInstance();
$beast->setHealth(new Health(60, 90));
$beast->setStrength(new Strength(60, 90));
$beast->setDefense(new Defense(40, 60));
$beast->setSpeed(new Speed(40, 60));
$beast->setLuck(new Luck(25, 40));

$battle = Game::getInstance($player, $beast);

try {
    $battle->battle();
} catch (\Exception $e) {
    print_r("ERROR: " . $e->getMessage());
}
